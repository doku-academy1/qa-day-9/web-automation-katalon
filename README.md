# web-automation-katalon

## Description
Terdapat 7 test case pada project ini dengan total 10 test scenario:
1. Test case Item Feature memiliki dua test scenario: test scenario untuk add item dan test scenario untuk remove item
2. Test case Login memiliki dua test scenario: test scenario untuk login success dan test scenario untuk login failed with username and password wrong
3. Test case Sort Feature memiliki dua test scenario: test scenario untuk sort product by name (Z to A) dan test scenario untuk sort product by price (low to high)
