<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>click sort</name>
   <tag></tag>
   <elementGuidId>4402e485-6094-4b34-8441-c74b6fbd9e32</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//select[@data-test='product_sort_container']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//select[@data-test='product_sort_container']</value>
      <webElementGuid>4033c476-8e5d-4ce2-a950-d2f41df471c0</webElementGuid>
   </webElementProperties>
</WebElementEntity>
