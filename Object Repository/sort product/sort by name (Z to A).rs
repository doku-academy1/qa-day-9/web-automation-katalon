<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>sort by name (Z to A)</name>
   <tag></tag>
   <elementGuidId>3e9306f0-c278-4bb5-a2b8-105d3213a61d</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//option[@value='za']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//option[@value='za']</value>
      <webElementGuid>e84e48ab-f919-4459-86d8-bb54941ce6ae</webElementGuid>
   </webElementProperties>
</WebElementEntity>
