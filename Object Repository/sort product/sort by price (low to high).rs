<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>sort by price (low to high)</name>
   <tag></tag>
   <elementGuidId>5c860159-16af-483f-8389-c47b91dd6fbd</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//option[@value='lohi']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//option[@value='lohi']</value>
      <webElementGuid>0a054bb3-897d-4233-be19-fbb02b959628</webElementGuid>
   </webElementProperties>
</WebElementEntity>
