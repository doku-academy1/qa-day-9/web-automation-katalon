<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>remove item</name>
   <tag></tag>
   <elementGuidId>3235f335-a14c-4b55-a1a9-2cb19a6fbd1d</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//button[@id='remove-sauce-labs-backpack']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//button[@id='remove-sauce-labs-backpack']</value>
      <webElementGuid>1e614f19-f5e0-41de-92eb-c5048f39637d</webElementGuid>
   </webElementProperties>
</WebElementEntity>
