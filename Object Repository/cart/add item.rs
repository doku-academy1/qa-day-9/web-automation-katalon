<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>add item</name>
   <tag></tag>
   <elementGuidId>6d4f5253-15a8-4ca4-bbb1-fc1cb3ebd524</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//button[@id='add-to-cart-sauce-labs-backpack']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//button[@id='add-to-cart-sauce-labs-backpack']</value>
      <webElementGuid>316d7231-eb45-43f0-b73c-379e82c19dad</webElementGuid>
   </webElementProperties>
</WebElementEntity>
