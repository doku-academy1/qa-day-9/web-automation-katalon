<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>page cart</name>
   <tag></tag>
   <elementGuidId>11e1dedf-7f77-4d4d-be6a-e3af726e5f2f</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//div[@id='shopping_cart_container']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//div[@id='shopping_cart_container']</value>
      <webElementGuid>0de27472-781c-43c0-bbfa-111bb53cd185</webElementGuid>
   </webElementProperties>
</WebElementEntity>
