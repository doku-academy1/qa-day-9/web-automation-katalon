<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>click menu button</name>
   <tag></tag>
   <elementGuidId>8199d10f-f285-4375-8b8f-873ad18146be</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//button[@id='react-burger-menu-btn']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//button[@id='react-burger-menu-btn']</value>
      <webElementGuid>65c6721c-c7b3-4236-b700-89cbedabbc97</webElementGuid>
   </webElementProperties>
</WebElementEntity>
