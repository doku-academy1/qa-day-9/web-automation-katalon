<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>click logout button</name>
   <tag></tag>
   <elementGuidId>144dff08-a12c-4700-86e4-a432200da377</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//a[@id='logout_sidebar_link']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//a[@id='logout_sidebar_link']</value>
      <webElementGuid>f430e7e7-f11e-4a95-8393-3481a0b06813</webElementGuid>
   </webElementProperties>
</WebElementEntity>
