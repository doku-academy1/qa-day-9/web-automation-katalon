<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>click about button</name>
   <tag></tag>
   <elementGuidId>dc20a537-c550-4a4e-bee3-7514f2dcea89</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//a[@id='about_sidebar_link']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//a[@id='about_sidebar_link']</value>
      <webElementGuid>a2d8f3b0-732d-471f-867e-acbfcca9bbfc</webElementGuid>
   </webElementProperties>
</WebElementEntity>
