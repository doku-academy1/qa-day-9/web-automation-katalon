<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input username</name>
   <tag></tag>
   <elementGuidId>2978dfcf-fe45-4793-ad8d-e380fd177729</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//input[@id='user-name']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//input[@id='user-name']</value>
      <webElementGuid>3b118cd5-d5d2-40cf-b24a-a6c561e6d767</webElementGuid>
   </webElementProperties>
</WebElementEntity>
