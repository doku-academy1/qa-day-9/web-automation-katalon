<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>click login</name>
   <tag></tag>
   <elementGuidId>92989c3a-9df2-47ec-bb64-a9ce04602287</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//input[@id='login-button']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//input[@id='login-button']</value>
      <webElementGuid>db29fcd6-2508-42f6-9d2d-5e3030ad954c</webElementGuid>
   </webElementProperties>
</WebElementEntity>
