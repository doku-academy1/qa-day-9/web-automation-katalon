<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>invalid username and password message</name>
   <tag></tag>
   <elementGuidId>885d3e92-4e6c-49ab-989b-57245f84014b</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//button[@class='error-button']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//button[@class='error-button']</value>
      <webElementGuid>22bb27da-5916-4e21-8a97-328556856b63</webElementGuid>
   </webElementProperties>
</WebElementEntity>
