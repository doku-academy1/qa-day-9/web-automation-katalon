<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>click twitter</name>
   <tag></tag>
   <elementGuidId>ff952121-eb92-49d5-8a58-6b40cb7d4be1</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//a[@href='https://twitter.com/saucelabs']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//a[@href='https://twitter.com/saucelabs']</value>
      <webElementGuid>074d2598-3d45-4d3f-ad35-4b2b07150b41</webElementGuid>
   </webElementProperties>
</WebElementEntity>
