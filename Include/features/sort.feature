#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template

Feature: Sort Feature
  I want to sort product by name and price

  
  Scenario Outline: Sort by name successfully
    Given I open Saucedemo
    When I field <username> and <password>
    When I success login
    Then I sort by name
    
    Examples: 
      | username  			| password | 
      | standard_user 	| secret_sauce |
  
  
  Scenario Outline: Sort by price successfully
    Given I open Saucedemo
    When I field <username> and <password>
    When I success login
    Then I sort by price
    
    Examples: 
      | username  			| password | 
      | standard_user 	| secret_sauce |